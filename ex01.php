<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
     $a = 10;
     $b = 20;
     $potencia = pow($b, $a);
     $potenciaFormatada =
       number_format($potencia, 0, ',', '.');
     echo $potenciaFormatada;
  ?>

  <table border='1'>
    <thead>
      <tr>
        <th>Operação</th>
        <th>Resultado</th>
      </tr>
      <tbody>
        <tr>
          <td>A + B</td>
          <td><?= ($a + $b); ?></td>
        </tr>
        <tr>
          <td>A - B</td>
          <td><?= ($a - $b); ?></td>
        </tr>
        <tr>
          <td>A x B</td>
          <td><?= ($a * $b); ?></td>
        </tr>
        <tr>
          <td>A / B</td>
          <td><?= ($a / $b); ?></td>
        </tr>
        <tr>
          <td>B<sup>A</sup></td>
          <td><?= $potenciaFormatada; ?></td>
        </tr>
        
      </tbody>
    </thead>
  </table>


</body>
</html>